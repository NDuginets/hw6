/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в
  браузерах, то нам понадобится сборщик который это умеет делать

  Выбор пал на вебпак.
  npm install --save-dev webpack

  Создадим файл конфига webpack.config.js

  База с документации
  const path = require('path');
  module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  };

  Затестим - в консоли наберем команду webpack
*/

// Для экспорта и импорта обязательно нужно имя



import Golub from './modules/2';

import Yastreb from './modules/3';

const newGolub = Golub('Саня белый');
console.log(newGolub);
newGolub.fly();
newGolub.eat();
newGolub.song();

const newYastreb = Yastreb('Четкий');
console.log(newYastreb);
newYastreb.fly();
newYastreb.eat();
newYastreb.hunt();

import CreatePlanet from './modules/5';

var Mars = CreatePlanet('Mars');
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();

var Earth = CreatePlanet('Earth');
Earth.rotatePlanet();


/*

  Установка webpack-dev-server
  npm install --save-dev webpack-dev-server
  -> автообвновления и пересборка


  package.json ->
    scripts -> "start": "webpack-dev-server --open"

  webpack.config.js ->
    devServer: {
      contentBase: './public'
    }

*/


/*
  Установим babel
  npm install babel-loader babel-core babel-preset-env --save-dev

  usage: https://webpack.js.org/loaders/babel-loader/#usage
  webpack.config.js ->
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  }

*/

/*
  Задание - разбить задание про птиц на модули и собрать в бандл при помощи вебпака.
*/
