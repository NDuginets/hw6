  /*

      Сегодня разберем некоторые паттерны проектирования JS приложений.

      Разберем паттерн "Фабрика" и то как он работает в JS.
      И какие проблемы он решает.

  */

    // class SpaceShipClass {
    //   constructor( name, position ){
    //
    //     this.name = name;
    //     this.position = position;
    //
    //     this.showYourPosition = this.showYourPosition.bind(this);
    //   }
    //
    //   flightTo( targetPlanet ){
    //     let FlightSpeed = '2000km/s';
    //     let { name } = this;
    //     console.log( `Ship ${name} flies to ${targetPlanet} at a speed of ` + FlightSpeed );
    //   }
    //   showYourPosition(){
    //     let {name, position} = this;
    //     console.log( this );
    //     console.log(`My name ${name}. Our position is ${ position[0] }, ${ position[1] }`);
    //   }
    // }
    //
    //
    // let myClassShip = new SpaceShipClass('X-wing', ['0', '25']);
    //     myClassShip.flightTo('Tattoin');

        // проверим что код корректно отработал.
        // console.log( myClassShip );

        // let ShootButton = document.getElementById('shotWithClass');
        // 1. Попробуем повесить метод
        // ShootButton.addEventListener('click',  myClassShip.showYourPosition);
        // Программа не работает - this ссылается на элемемнт на который мы кликнули

        // 2. Попробуем указать на обьект через bind(obj)
        // ShootButton.addEventListener('click',  myClassShip.showYourPosition.bind(myClassShip));
        // Такой вариант работает, но выглядит уныло :(

        // 3. Забиндим обработчику обьект в конструкторе
        // ShootButton.addEventListener('click',  myClassShip.showYourPosition);
        // Работает, но тоже не выглядит как best practice

        // 4. Arrow Functions
        // ShootButton.addEventListener('click',  () => myClassShip.showYourPosition() );
        // Тоже вариант решения, так как стрелочные функции не переопределяют контекст this

        /*

          Проблема решаема, но возникает на пустом месте.
          Давайте рассмотрим другой архитектурный подход.
          Фабрика - это простая функция, которая порождает новый обьект.

        */

    /*
    const SpaceShip = ( name, position ) => {
      // Приватнное свойсвто!
      let FlightSpeed = '2000km/s';

      return {
        name: name,
        position: position,
        // Add capitan form _.js
        flightTo: ( targetPlanet ) => console.log( `Ship ${name} flies to ${targetPlanet} at a speed of ` + FlightSpeed ),
        showYourPosition: () => {
          // Можно использовать как обычные, так и стрелочные функции
          // Не нужно использовать деконстркуцию, обращаемся напрямую к свойствам
          // Не нужно использовать this что бы обратиться к свойствам
          console.log(`My name ${name}. Our position is ${ position[0] }, ${ position[1] }`);
        }
      };
    };
    // Проверим работу
    const myShip = SpaceShip("X-wing", ["25", "35"]);
          myShip.flightTo('Earth');

    const myShip2 = SpaceShip("Millennium Falcon", ["1", "0"]);
          myShip2.flightTo('Endor');
          myShip2.showYourPosition();
    // Проверим работу класса вызвав публичное свойство name
    console.log( myShip2 );
    // Проверим работу обработчика
    ShootButton.addEventListener('click',  myShip.showYourPosition);
    // Done.

    */
    
    /*

      Итог - фабрика очень полезный паттерн проектирования, который:

      1. + Является простой функцией которая создает обьекты
      2. + Решает проблемы с контекстом выполнения методов обьекта
      3. + Позволяет использовать приватные функции и свойста
      4. + Является полноценной заменой классу

      5. - Является более медленной заменой класам. По причине того, что
      все переменные и методы инициализируются каждый раз при вызове фабрики.
      Минус очень абстрактный, так как пример выше с использованием фабрики
      генерируется за 0.00004 ms, обьект созданый через класс генерируется за
      0.00002 ms - т.е в 2 раза быстрее.
      Эффект такого может быть заметен если вы допустим заходите сгенерировать
      10000 новых обьектов через фабрику. Выполнение такого скрипта займет
      2ms через класс и 4ms черещ фабрику соответствено.
      Другой вопрос, в том, зачем нужно генерировать за 1 итерацию 10000
      обьектов ¯\_(ツ)_/¯

    */


    /*
      Задание:

      Написать фабрику, которая создает планеты.
        Есть публичные свойства и методы:
          name: "",
          population: randomPopulation(),
          timeCycle: 24,

          growPopulation: () => {
            функция которая берет приватное свойство populationMultiplyRate
            которое равняется случайному числу от 1 до 10 и умножает его на 1/1000 от популяции.
            Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
            что за один цикл прибавилось столько населения планеты.
          }
          rotatePlanet: () => {
            функция которая на основе зарандомленого (Math.round) числа делает действие.
            вначале рандомим случайное число. Если число делится на 2 без остачи - запускаем метод
            growPopulation. Если без остачи не делиться - то запускаем приватный метод Cataclysm


          }

        Приватные свойства и методы:
          randomPopulation: () => Возвращает случайное целое число от 1.000.000 до 100.000.000
          populationMultiplyRate - случайное число от 1 до 10
          Cataclysm: () => {
            Рандомим число от 1 до 10 и умножаем его на 10000;
            То число которое получили, отнимаем от популяции.
            В консоль выводим сообщение - от катаклизма погибло Х человек.
          }


    */
