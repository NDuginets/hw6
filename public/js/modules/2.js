import { Run, Fly, Swimm, Eat, Song, Hunt } from './1.js';

const Golub = (name) => {
  let state = {
    name,
    speed: 100,
    food: 'shit',
    music: 'Kyrluk-Kyrluc'
  };

  return Object.assign(
    {},
    state,
    Fly(state),
    Eat(state),
    Song(state)
  );
};

export default Golub;
