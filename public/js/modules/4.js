  const PopulationMultiplyRate =  (state ) => ({
    populationMultiplyRate: () => Math.floor(Math.random() * (10 - 1 + 1)) + 1
  });
  const  GrowPopulation = (state) => ({
    growPopulation: () => {
    console.log(`Количество людей на планете ${state.name} : ${state.population}`);
    let newPeople = (populationMultiplyRate ())*(state.population/1000);
    state.population = state.population + newPeople;
    console.log(`Люди по ночам сделали ${newPeople} новых людей`);
    console.log('Текущее население планеты:', state.population);} 
  });
  const RandomPopulation = ( state ) => ({
    randomPopulation: () => Math.floor(Math.random() * (100000000 - 1000000 + 1)) + 100000
  });
  const Cataclysm = (state) => ({
     cataclysm: () => {  
      let x = (Math.floor(Math.random() * (10 - 1 + 1)) + 1)*10;
      console.log(`Количество людей на планете ${state.name} : ${state.population}`);
      console.log(`Упс...от катаклизма "Клизма" умерло ${x} людей.`);
      state.population = state.population - x;
      console.log('Осталось людей:', state.population);}
  });

  export {PopulationMultiplyRate, GrowPopulation, RandomPopulation, Cataclysm};