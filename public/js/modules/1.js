const Run = ( state ) => ({
    run: () => console.log(`This ${state.name} can run with speed ${state.speed} `)
  });

const Fly = ( state ) => ({
    fly: () => console.log(`This ${state.name} can fly with speed ${state.speed} `)
  });

const Swimm = ( state ) => ({
    swimm: () => console.log(`This ${state.name} can swimm with speed ${state.speed} `)
  });

const Eat = ( state ) => ({
    eat: () => console.log(`This ${state.name} can eat ${state.food} `)
  });
  
const Song = ( state ) => ({
    song: () => console.log(`This ${state.name} can song ${state.music} `)
  });

const Hunt = ( state ) => ({
    hunt: () => console.log(`This ${state.name} can hant ${state.kill} `)
  });


export {Run, Fly, Swimm, Eat, Song, Hunt};
