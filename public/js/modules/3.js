import { Run, Fly, Swimm, Eat, Song, Hunt } from './1.js';


const Yastreb = (name) => {
  let state = {
    name,
    speed: 99999999,
    food: 'meat',
    kill: 'other birds'
  };

  return Object.assign(
    {},
    Fly(state),
    Eat(state),
    Hunt(state)
  );
};

export default Yastreb;
