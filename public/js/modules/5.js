import {PopulationMultiplyRate, GrowPopulation, RandomPopulation, Cataclysm} from './4.js';

const CreatePlanet = ( name ) => {

          // Object to return
          const state = {
            name,
            population: RandomPopulation(),
            rotatePlanet: () => {
              let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
              if ( (randomNumber % 2) == 0) {
                GrowPopulation(state);
              } else {
                Cataclysm(state);
              }
            }
          };
          return state;
    };
    

    