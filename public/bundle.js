/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var Run = function Run(state) {
    return {
        run: function run() {
            return console.log("This " + state.name + " can run with speed " + state.speed + " ");
        }
    };
};

var Fly = function Fly(state) {
    return {
        fly: function fly() {
            return console.log("This " + state.name + " can fly with speed " + state.speed + " ");
        }
    };
};

var Swimm = function Swimm(state) {
    return {
        swimm: function swimm() {
            return console.log("This " + state.name + " can swimm with speed " + state.speed + " ");
        }
    };
};

var Eat = function Eat(state) {
    return {
        eat: function eat() {
            return console.log("This " + state.name + " can eat " + state.food + " ");
        }
    };
};

var Song = function Song(state) {
    return {
        song: function song() {
            return console.log("This " + state.name + " can song " + state.music + " ");
        }
    };
};

var Hunt = function Hunt(state) {
    return {
        hunt: function hunt() {
            return console.log("This " + state.name + " can hant " + state.kill + " ");
        }
    };
};

exports.Run = Run;
exports.Fly = Fly;
exports.Swimm = Swimm;
exports.Eat = Eat;
exports.Song = Song;
exports.Hunt = Hunt;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _ = __webpack_require__(2);

var _2 = _interopRequireDefault(_);

var _3 = __webpack_require__(3);

var _4 = _interopRequireDefault(_3);

var _5 = __webpack_require__(4);

var _6 = _interopRequireDefault(_5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в
  браузерах, то нам понадобится сборщик который это умеет делать

  Выбор пал на вебпак.
  npm install --save-dev webpack

  Создадим файл конфига webpack.config.js

  База с документации
  const path = require('path');
  module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  };

  Затестим - в консоли наберем команду webpack
*/

// Для экспорта и импорта обязательно нужно имя


var newGolub = (0, _2.default)('Саня белый');
console.log(newGolub);
newGolub.fly();
newGolub.eat();
newGolub.song();

var newYastreb = (0, _4.default)('Четкий');
console.log(newYastreb);
newYastreb.fly();
newYastreb.eat();
newYastreb.hunt();

var Mars = (0, _6.default)('Mars');
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();

var Earth = (0, _6.default)('Earth');
Earth.rotatePlanet();

/*

  Установка webpack-dev-server
  npm install --save-dev webpack-dev-server
  -> автообвновления и пересборка


  package.json ->
    scripts -> "start": "webpack-dev-server --open"

  webpack.config.js ->
    devServer: {
      contentBase: './public'
    }

*/

/*
  Установим babel
  npm install babel-loader babel-core babel-preset-env --save-dev

  usage: https://webpack.js.org/loaders/babel-loader/#usage
  webpack.config.js ->
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  }

*/

/*
  Задание - разбить задание про птиц на модули и собрать в бандл при помощи вебпака.
*/

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = __webpack_require__(0);

var Golub = function Golub(name) {
  var state = {
    name: name,
    speed: 100,
    food: 'shit',
    music: 'Kyrluk-Kyrluc'
  };

  return Object.assign({}, state, (0, _.Fly)(state), (0, _.Eat)(state), (0, _.Song)(state));
};

exports.default = Golub;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = __webpack_require__(0);

var Yastreb = function Yastreb(name) {
  var state = {
    name: name,
    speed: 99999999,
    food: 'meat',
    kill: 'other birds'
  };

  return Object.assign({}, (0, _.Fly)(state), (0, _.Eat)(state), (0, _.Hunt)(state));
};

exports.default = Yastreb;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _ = __webpack_require__(5);

var CreatePlanet = function CreatePlanet(name) {

  // Object to return
  var state = {
    name: name,
    population: RandomPopulation(),
    rotatePlanet: function rotatePlanet() {
      var randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
      if (randomNumber % 2 == 0) {
        GrowPopulation(state);
      } else {
        (0, _.Cataclysm)(state);
      }
    }
  };
  return state;
};

/***/ }),
/* 5 */
/***/ (function(module, exports) {

throw new Error("Module build failed: SyntaxError: C:/Users/faust/Desktop/Where/Prog/Js_prof/LS7/lesson_7/public/js/modules/4.js: Unexpected token, expected , (2:78)\n\n  1 |   const PopulationMultiplyRate =  (state ) => ({\n> 2 |     populationMultiplyRate: () => Math.floor(Math.random() * (10 - 1 + 1)) + 1;\n    |                                                                               ^\n  3 |   });\n  4 |   const  GrowPopulation = (state) => ({\n  5 |     growPopulation: () => {\n");

/***/ })
/******/ ]);